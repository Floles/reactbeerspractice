import React, { Component} from "react";
import {hot} from "react-hot-loader";
import "./App.css";
import Navbar from "./Navbar/Navbar";
import BodyBeer from "./BodyBeer";


class App extends Component{
    render(){
        return(
        <div className="App">
            <Navbar/>
            <BodyBeer/>
        </div>
        );
    }
}

export default hot(module)(App);