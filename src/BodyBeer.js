import React, { Component} from "react";
import axios from "axios";
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import blue from '@material-ui/core/colors/blue';

const styles = {
    root: {
        flexGrow: 1,
    },
};

class BodyBeer extends Component {
    constructor(props){
        super(props)

        this.displayBeers = this.displayBeers.bind(this)

        this.state = {
            beer : []
        }
    }

    getBeers(){
        const url = "https://api.punkapi.com/v2/beers"
        axios.get(url)
            .then(response => {
                console.log(response);
                this.setState({ beer : response.data });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    componentDidMount(){
        this.getBeers()
    }

    displayBeers(){
       
        return (
            <div>
                {
                    this.state.beer.map((mybeer, key) => 
                        <div key={key}>
                        <Grid item xs={6}>
                            <img style={{width:'5%'}} src={mybeer.image_url}></img>
                        </Grid>
                        <Grid item xs={6} key={key}>
                            <div>{mybeer.name}</div>
                        </Grid>
                    </div>
                    )
                }
            </div>
        )
    }

    render() {

        return ( 
            <div>
                <h1 style={{textAlign: 'center'}}> Venez découvrir les bières du monde !</h1>
                <Button variant="contained" style={{ backgroundColor:'#2196f3', color:'white'}} > Hello World </ Button> 
                <div style={{ marginTop: 20 }}>
                    {this.displayBeers()}
                </div>
             </div>
        );
    }
}

export default withStyles(styles)(BodyBeer);